﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lesson7._2
{ 
class Program
{
    static void Main()
    {
        Product[] products = new Product[]
        {
            new Product("Paper Towels", 100.0),
            new Product("Gel Detergent", 80.0),
            new Product("Face Cream", 300.0)
        };
        ShoppingCart cart = new ShoppingCart(products);
        DiscountStrategy percentageDiscount = new PercentageDiscount(15);
        DiscountStrategy fixedDiscount = new FixedDiscount(45);

        cart.SetDiscountStrategy(percentageDiscount);

        double totalWithDiscount = cart.CalculateTotal();
        Console.WriteLine($"Your total amount after percent discount: {totalWithDiscount}");

        cart.SetDiscountStrategy(fixedDiscount);

        double totalWithFixedDiscount = cart.CalculateTotal();
        Console.WriteLine($"Your total amount after fixed discount: {totalWithFixedDiscount}");
    }
}
}

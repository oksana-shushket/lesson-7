﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lesson7._2
{
    class PercentageDiscount : DiscountStrategy
    {
        private double fixedPercent;

        public PercentageDiscount(double fixedPercent)
        {
            this.fixedPercent = fixedPercent;
        }

        public override double ApplyDiscount(double originalPrice)
        {
            return originalPrice - ((originalPrice / 100) * fixedPercent);
        }
    }
}

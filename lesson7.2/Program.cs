﻿using lesson7._2;

public abstract class DiscountStrategy
{
    public abstract double ApplyDiscount(double originalPrice);   
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lesson7._2
{
    class ShoppingCart
    {
        public DiscountStrategy discountStrategy;
        public Product[] products;
        public ShoppingCart(Product[] products)
        {
            this.products = products;
            discountStrategy = null;
        }
        public void SetDiscountStrategy(DiscountStrategy strategy)
        {
            discountStrategy = strategy;
        }
        public double CalculateTotal()
        {
            double total = 0;
            foreach (var product in products)
            {
                total += discountStrategy.ApplyDiscount(product.Price);
            }
            return total;
        }
    }
}

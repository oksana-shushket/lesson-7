﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lessom7._1
{
    class PetShop
    {
        private Animal[] animals;

        public PetShop(Animal[] animals)
        {
            this.animals = animals;
        }

        public void MakeAllSounds()
        {
            Console.WriteLine("Our animals sound like that:");

            foreach (var animal in animals)
            {
                animal.MakeSound();
            }
        }
    }
}

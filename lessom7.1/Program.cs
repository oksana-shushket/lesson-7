﻿using lessom7._1;

class Program
{
    static void Main()
    {
        Animal[] animals = new Animal[]
        {
            new Dog(),
            new Cat(),
        };

        PetShop petShop = new PetShop(animals);
        petShop.MakeAllSounds();
    }
}
